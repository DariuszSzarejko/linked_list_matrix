#ifndef MATRIX_H
#define MATRIX_H
#include "List.h"
class Matrix
{
public:
    Matrix ();
    ~Matrix ();
    double CalcDeterminant (double *mat, int order);
    void sub (LinkedList *A, LinkedList *B, LinkedList *C);
    void add (LinkedList *A, LinkedList *B, LinkedList *C);
    void multiply (LinkedList *A, LinkedList *B, LinkedList *C);
    int GetMinor (double *src, double *dest, int row, int col, int order);
    double calcDet (LinkedList *mat);
    void MatrixInversion (double *A, int order, double *Y);
    void matrixInv (LinkedList *mat, LinkedList *mat_inv);
private:

};

#endif /* MATRIX_H */

