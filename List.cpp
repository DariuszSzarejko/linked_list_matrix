#include "List.h"

LinkedList::LinkedList ()
{
    this->length = 0;
    this->head = NULL;
}

LinkedList::~LinkedList ()
{
    
}

void LinkedList::addFront (double data)
{
    Node* node = new Node ();
    node->data = data;
    node->next = this->head;
    this->head = node;

    this->length++;
}

void LinkedList::print ()
{
    Node* head = this->head;
    for (unsigned int j = 0; j<this->height; j++)
    {
        for (unsigned int i = 0; i < this->width; i++)
        {
            std::cout << head->data << "   ";
            head = head->next;
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
    //int i = 1;
    //    while (head)
    //    {
    //        //        std::cout << i << ": " << head->data << std::endl;
    //
    //
    //
    //        //        i++;
    //    }
}

void LinkedList::addBack (double addData)
{
    Node* node = new Node; //create new node
    node->data = addData; //input data
    node->next = NULL; //set node to point to NULL

    if (tail == NULL) 
        head = node;
    else
        tail->next = node;
    tail = node;
    length++;
}

void LinkedList::removeFront ()
{
    if (head != NULL)
    {
        if (tail == head) tail = NULL;
        Node* node = head;
        head = head->next;
        delete node;
        length--;
    }
}

Node* LinkedList::getNthElement (unsigned int n)
{
    Node *head = this->head;

    unsigned int i = 0;
    while (head)
    {
        if (i == n)
            return head;
        head = head->next;
        i++;
    }
    return NULL;
}

double LinkedList::getValue (unsigned int x, unsigned int y)
{
    unsigned int n = y * width + x;
    Node *node = getNthElement (n);
    return node->data;
}