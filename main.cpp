#include <iostream>
#include <fstream>
#include <algorithm>
#include <string>
#include "List.h"
#include "Matrix.h"
#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */

void readMatrix (LinkedList **list_arr, int &n)
{
    std::ifstream input_file ("input.txt", ios::in);
    std::string delimiter = ",";
    size_t pos = 0;
    std::string::size_type sz;
    double value = 0;
    int i = 0;

    if (input_file.good ())
    {
        std::string line;
        while (std::getline (input_file, line))
        {
            //line.erase (std::remove (line.begin (), line.end (), '\n'), line.end ());
            LinkedList *list = new LinkedList ();
            std::cout << "Reading matrix: " << i << std::endl;
            pos = 0;
            std::string token;
            int counter = 0;
            while ((pos = line.find (delimiter)) != std::string::npos)
            {

                token = line.substr (0, pos);
                if (counter < 2)
                {
                    unsigned int dim = atoi (token.c_str ());
                    if (counter == 0)
                        list->width = dim;
                    else if (counter == 1)
                        list->height = dim;
                    counter++;
                    line.erase (0, pos + delimiter.length ());
                    continue;
                }
                value = std::stod (token, &sz);
                list->addBack (value); // Add element to list
                line.erase (0, pos + delimiter.length ());
            }
            value = std::stod (line, &sz);
            list->addBack (value); // Add element to list
            // Saving pointer to array
            list_arr[i] = list;
            i++;
        }
        n = i;
        input_file.close ();
    }
    else
    {
    }
}

int main (int argc, char const *argv[])
{
    std::cout << "Podaj operacje:" << std::endl;
    std::cout << "\t1: dodawanie" << std::endl;
    std::cout << "\t2: odejmowanie" << std::endl;
    std::cout << "\t3: mnozenie" << std::endl;
    std::cout << "\t4: odwrotna" << std::endl;
    std::cout << "\t5: wyznacznik" << std::endl;
    std::cout << "$: ";
    std::string ans1;
    std::cin >> ans1;
    if (ans1 == "1")
    {
        std::cout << "Dodawanie" << std::endl;
        LinkedList **list_arr = new LinkedList*[2];
        LinkedList *C = new LinkedList ();
        Matrix matrix;

        int n = 0;
        readMatrix (list_arr, n);
        list_arr[0]->print ();
        list_arr[1]->print ();

        matrix.add (list_arr[0], list_arr[1], C);
        C->print ();
    }
    else if (ans1 == "2")
    {
        std::cout << "Odejmowanie" << std::endl;
        LinkedList **list_arr = new LinkedList*[2];
        LinkedList *C = new LinkedList ();
        Matrix matrix;

        int n = 0;
        readMatrix (list_arr, n);
        list_arr[0]->print ();
        list_arr[1]->print ();

        matrix.sub (list_arr[0], list_arr[1], C);
        C->print ();
    }
    else if (ans1 == "3")
    {
        std::cout << "Mnozenie" << std::endl;
        LinkedList **list_arr = new LinkedList*[2];
        LinkedList *C = new LinkedList ();
        Matrix matrix;

        int n = 0;
        readMatrix (list_arr, n);
        list_arr[0]->print ();
        list_arr[1]->print ();
        matrix.multiply (list_arr[0], list_arr[1], C);
        C->print ();
    }
    else if (ans1 == "4")
    {
        std::cout << "Odwrotna" << std::endl;
        LinkedList **list_arr = new LinkedList*[2];
        LinkedList *m_inv = new LinkedList ();
        Matrix matrix;
        int n = 0;
        readMatrix (list_arr, n);
        list_arr[0]->print ();
        matrix.matrixInv (list_arr[0], m_inv);
        m_inv->print ();
    }
    else if (ans1 == "5")
    {
        std::cout << "Wyznacznik" << std::endl;
        LinkedList **list_arr = new LinkedList*[2];
        Matrix matrix;
        int n = 0;
        readMatrix (list_arr, n);
        list_arr[0]->print ();
        double det = matrix.calcDet (list_arr[0]);
        std::cout << "Wyznacznik = " << det << std::endl;
    }



    //sub (list_arr[0], list_arr[1]);
    //list_arr[0]->print ();

    //std::cout << list_arr[0]->getValue (2, 1) << std::endl;
    //std::cout << calcDet (list_arr[0]) << std::endl;

    //    m.matrixInv (list_arr[0], m_inv);
    //    std::cout << "Length: " << m_inv->length << std::endl;
    //    m_inv->print ();

    //std::cout << list_arr[0]->getNthElement (0)->data << std::endl;
    //std::cout << list_arr[0]->getNthElement (1)->data << std::endl;
    return 0;
}
// https://chi3x10.wordpress.com/2008/05/28/calculate-matrix-inversion-in-c/