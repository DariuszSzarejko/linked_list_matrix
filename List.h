/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   List.h
 * Author: darek
 *
 * Created on March 19, 2018, 6:59 PM
 */

#ifndef LIST_H
#define LIST_H
#include <iostream>
//class List
//{
//public:
//    List ();
//    List (const List& orig);
//    virtual ~List ();
//private:
//
//};

using namespace std;

class Node
{
public:
    Node* next;
    double data;
};

class LinkedList
{
public:
    LinkedList ();
    ~LinkedList ();
    unsigned int length;
    unsigned int width;
    unsigned int height;
    Node* head;
    Node* tail;

    void addFront (double data);
    void addBack (double addData);
    void removeFront ();
    Node* getNthElement (unsigned int n);
    double getValue (unsigned int x, unsigned int y);
    void print ();
};

#endif /* LIST_H */

