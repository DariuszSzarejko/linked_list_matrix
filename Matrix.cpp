#include "Matrix.h"

Matrix::Matrix () { }

Matrix::~Matrix () { }

void Matrix::multiply (LinkedList *A, LinkedList *B, LinkedList *C)
{
    if (A->width == B->height)
    {
        C->height = A->height;
        C->width = B->width;

        // Copy
        double *a = new double [A->length];
        double *b = new double [B->length];

        //A
        Node* head = A->head;
        int i = 0;
        while (head)
        {
            a[i] = head->data;
            head = head->next;
            i++;
        }
        //B
        head = B->head;
        i = 0;
        while (head)
        {
            b[i] = head->data;
            head = head->next;
            i++;
        }
        double temp = 0;

        //C = A*B
        for (int y = 0; y < C->height; y++)
        {
            for (int x = 0; x < C->width; x++)
            {
                for (int k = 0; k < B->height; k++)
                {
                    temp += a[y * A->width + k] * b[k * B->width + x];
                }
                C->addBack (temp);
                temp = 0;
            }
        }
        delete[] a;
        delete[] b;
    }
}

void Matrix::add (LinkedList *A, LinkedList *B, LinkedList *C)
{
    if ((A->width == B->width)&&(A->height == B->height))
    {
        C->height = A->height;
        C->width = A->width;
        Node* head_1 = A->head;
        Node* head_2 = B->head;
        while (head_1)
        {
            C->addBack (head_1->data + head_2->data);
            head_1 = head_1->next;
            head_2 = head_2->next;
        }
    }
}

void Matrix::sub (LinkedList *A, LinkedList *B, LinkedList *C)
{
    if ((A->width == B->width)&&(A->height == B->height))
    {
        C->height = A->height;
        C->width = A->width;
        Node* head_1 = A->head;
        Node* head_2 = B->head;
        while (head_1)
        {
            C->addBack (head_1->data - head_2->data);
            head_1 = head_1->next;
            head_2 = head_2->next;
        }
    }
}

int Matrix::GetMinor (double *src, double *dest, int row, int col, int order)
{
    // indicate which col and row is being copied to dest
    int colCount = 0, rowCount = 0;

    for (int j = 0; j < order; j++)
    {
        if (j != row)
        {
            colCount = 0;
            for (int i = 0; i < order; i++)
            {
                // when j is not the element
                if (i != col)
                {
                    dest[rowCount * (order - 1) + colCount] = src[j * order + i];
                    colCount++;
                }
            }
            rowCount++;
        }
    }
    return 1;
}

// Calculate the determinant recursively.

double Matrix::CalcDeterminant (double *mat, int order)
{
    // order must be >= 0
    // stop the recursion when matrix is a single element
    if (order == 1)
        return mat[0];

    // the determinant value
    double det = 0;

    // allocate the cofactor matrix
    double *minor = new double[(order - 1) * (order - 1)];

    for (int i = 0; i < order; i++)
    {
        // get minor of element (0,i)
        GetMinor (mat, minor, 0, i, order);
        // the recusion is here!
        det += (i % 2 == 1 ? -1.0 : 1.0) * mat[i] * CalcDeterminant (minor, order - 1);
    }
    delete [] minor;
    return det;
}

double Matrix::calcDet (LinkedList *mat)
{
    double det = 0.0;
    if (mat->width == mat->height)
    {
        // Copy
        double *m = new double [mat->length];
        Node *head = mat->head;
        unsigned int i = 0;
        while (head)
        {
            m[i] = head->data;
            head = head->next;
            i++;
        }
        det = CalcDeterminant (m, mat->height);
        delete[] m;
    }
    return det;
}

void Matrix::MatrixInversion (double *A, int order, double *Y)
{
    // get the determinant of a
    double det = 1.0 / CalcDeterminant (A, order);

    // memory allocation
    //    double *temp = new double[(order - 1)*(order - 1)];

    double *minor = new double[(order - 1) * (order - 1)];

    for (int j = 0; j < order; j++)
    {
        for (int i = 0; i < order; i++)
        {
            // get the co-factor (matrix) of A(j,i)
            GetMinor (A, minor, j, i, order);
            Y[j * order + i] = det * CalcDeterminant (minor, order - 1);
            if ((i + j * order) % 2 == 1)
                Y[j * order + i] = -Y[j * order + i];
        }
    }
    delete [] minor;
}

void Matrix::matrixInv (LinkedList *mat, LinkedList *mat_inv)
{
    if (mat->width == mat->height)
    {
        mat_inv->height = mat->height;
        mat_inv->width = mat->width;
        // Copy
        double *m = new double [mat->length];
        double *y = new double [mat->length];
        Node *head = mat->head;
        unsigned int i = 0;
        while (head)
        {
            m[i] = head->data;
            head = head->next;
            i++;
        }
        MatrixInversion (m, mat->height, y);
        // Copy
        for (int k = 0; k < mat->length; k++)
            mat_inv->addBack ((double) y[k]);
        delete[] m;
        delete[] y;
    }
    else
    {

    }
}
